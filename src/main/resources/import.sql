INSERT INTO country (country_name, multiplier) VALUES ('Andorra', 1.104);
INSERT INTO country (country_name, multiplier) VALUES ('United Arab Emirates', 1.27);
INSERT INTO country (country_name, multiplier) VALUES ('Afghanistan', 1.256);
INSERT INTO country (country_name, multiplier) VALUES ('Antigua and Barbuda', 1.38);
INSERT INTO country (country_name, multiplier) VALUES ('Anguilla', 1.378);
INSERT INTO country (country_name, multiplier) VALUES ('Albania', 1.112);
INSERT INTO country (country_name, multiplier) VALUES ('Armenia', 1.166);
INSERT INTO country (country_name, multiplier) VALUES ('Netherlands Antilles', 1.425);
INSERT INTO country (country_name, multiplier) VALUES ('Angola', 1.408);
INSERT INTO country (country_name, multiplier) VALUES ('Antarctica', 1.773);
INSERT INTO country (country_name, multiplier) VALUES ('Argentina', 1.66);
INSERT INTO country (country_name, multiplier) VALUES ('American Samoa', 1.768);
INSERT INTO country (country_name, multiplier) VALUES ('Austria', 1.072);
INSERT INTO country (country_name, multiplier) VALUES ('Australia', 1.732);
INSERT INTO country (country_name, multiplier) VALUES ('Aruba', 1.426);
INSERT INTO country (country_name, multiplier) VALUES ('Azerbaijan', 1.172);
INSERT INTO country (country_name, multiplier) VALUES ('Bosnia and Herzegovina', 1.095);
INSERT INTO country (country_name, multiplier) VALUES ('Barbados', 1.393);
INSERT INTO country (country_name, multiplier) VALUES ('Bangladesh', 1.369);
INSERT INTO country (country_name, multiplier) VALUES ('Belgium', 1.057);
INSERT INTO country (country_name, multiplier) VALUES ('Burkina Faso', 1.277);
INSERT INTO country (country_name, multiplier) VALUES ('Bulgaria', 1.111);
INSERT INTO country (country_name, multiplier) VALUES ('Bahrain', 1.249);
INSERT INTO country (country_name, multiplier) VALUES ('Burundi', 1.372);
INSERT INTO country (country_name, multiplier) VALUES ('Benin', 1.291);
INSERT INTO country (country_name, multiplier) VALUES ('Bermuda', 1.316);
INSERT INTO country (country_name, multiplier) VALUES ('Brunei', 1.531);
INSERT INTO country (country_name, multiplier) VALUES ('Bolivia', 1.551);
INSERT INTO country (country_name, multiplier) VALUES ('Brazil', 1.511);
INSERT INTO country (country_name, multiplier) VALUES ('Bahamas', 1.386);
INSERT INTO country (country_name, multiplier) VALUES ('Bhutan', 1.351);
INSERT INTO country (country_name, multiplier) VALUES ('Bouvet Island', 1.654);
INSERT INTO country (country_name, multiplier) VALUES ('Botswana', 1.474);
INSERT INTO country (country_name, multiplier) VALUES ('Belarus', 1.064);
INSERT INTO country (country_name, multiplier) VALUES ('Belize', 1.455);
INSERT INTO country (country_name, multiplier) VALUES ('Canada', 1.307);
INSERT INTO country (country_name, multiplier) VALUES ('Cocos [Keeling] Islands', 1.563);
INSERT INTO country (country_name, multiplier) VALUES ('Congo [DRC]', 1.369);
INSERT INTO country (country_name, multiplier) VALUES ('Central African Republic', 1.308);
INSERT INTO country (country_name, multiplier) VALUES ('Congo [Republic]', 1.344);
INSERT INTO country (country_name, multiplier) VALUES ('Switzerland', 1.075);
INSERT INTO country (country_name, multiplier) VALUES ('Côte d`Ivoire', 1.307);
INSERT INTO country (country_name, multiplier) VALUES ('Cook Islands', 1.804);
INSERT INTO country (country_name, multiplier) VALUES ('Chile', 1.667);
INSERT INTO country (country_name, multiplier) VALUES ('Cameroon', 1.3);
INSERT INTO country (country_name, multiplier) VALUES ('China', 1.349);
INSERT INTO country (country_name, multiplier) VALUES ('Colombia', 1.477);
INSERT INTO country (country_name, multiplier) VALUES ('Costa Rica', 1.479);
INSERT INTO country (country_name, multiplier) VALUES ('Cuba', 1.404);
INSERT INTO country (country_name, multiplier) VALUES ('Cape Verde', 1.289);
INSERT INTO country (country_name, multiplier) VALUES ('Christmas Island', 1.58);
INSERT INTO country (country_name, multiplier) VALUES ('Cyprus', 1.165);
INSERT INTO country (country_name, multiplier) VALUES ('Czech Republic', 1.06);
INSERT INTO country (country_name, multiplier) VALUES ('Germany', 1.05);
INSERT INTO country (country_name, multiplier) VALUES ('Djibouti', 1.306);
INSERT INTO country (country_name, multiplier) VALUES ('Denmark', 1);
INSERT INTO country (country_name, multiplier) VALUES ('Dominica', 1.387);
INSERT INTO country (country_name, multiplier) VALUES ('Dominican Republic', 1.396);
INSERT INTO country (country_name, multiplier) VALUES ('Algeria', 1.186);
INSERT INTO country (country_name, multiplier) VALUES ('Ecuador', 1.52);
INSERT INTO country (country_name, multiplier) VALUES ('Estonia', 1.042);
INSERT INTO country (country_name, multiplier) VALUES ('Egypt', 1.205);
INSERT INTO country (country_name, multiplier) VALUES ('Western Sahara', 1.225);
INSERT INTO country (country_name, multiplier) VALUES ('Eritrea', 1.283);
INSERT INTO country (country_name, multiplier) VALUES ('Spain', 1.123);
INSERT INTO country (country_name, multiplier) VALUES ('Ethiopia', 1.317);
INSERT INTO country (country_name, multiplier) VALUES ('Finland', 1.043);
INSERT INTO country (country_name, multiplier) VALUES ('Fiji', 1.776);
INSERT INTO country (country_name, multiplier) VALUES ('Falkland Islands [Islas Malvinas]', 1.715);
INSERT INTO country (country_name, multiplier) VALUES ('Micronesia', 1.603);
INSERT INTO country (country_name, multiplier) VALUES ('Faroe Islands', 1.05);
INSERT INTO country (country_name, multiplier) VALUES ('France', 1.083);
INSERT INTO country (country_name, multiplier) VALUES ('Gabon', 1.347);
INSERT INTO country (country_name, multiplier) VALUES ('United Kingdom', 1.05);
INSERT INTO country (country_name, multiplier) VALUES ('Grenada', 1.403);
INSERT INTO country (country_name, multiplier) VALUES ('Georgia', 1.152);
INSERT INTO country (country_name, multiplier) VALUES ('French Guiana', 1.422);
INSERT INTO country (country_name, multiplier) VALUES ('Guernsey', 1.074);
INSERT INTO country (country_name, multiplier) VALUES ('Ghana', 1.301);
INSERT INTO country (country_name, multiplier) VALUES ('Gibraltar', 1.148);
INSERT INTO country (country_name, multiplier) VALUES ('Greenland', 1.136);
INSERT INTO country (country_name, multiplier) VALUES ('Gambia', 1.287);
INSERT INTO country (country_name, multiplier) VALUES ('Guinea', 1.299);
INSERT INTO country (country_name, multiplier) VALUES ('Guadeloupe', 1.381);
INSERT INTO country (country_name, multiplier) VALUES ('Equatorial Guinea', 1.333);
INSERT INTO country (country_name, multiplier) VALUES ('Greece', 1.126);
INSERT INTO country (country_name, multiplier) VALUES ('South Georgia and the South Sandwich Islands', 1.688);
INSERT INTO country (country_name, multiplier) VALUES ('Guatemala', 1.467);
INSERT INTO country (country_name, multiplier) VALUES ('Guam', 1.56);
INSERT INTO country (country_name, multiplier) VALUES ('Guinea-Bissau', 1.296);
INSERT INTO country (country_name, multiplier) VALUES ('Guyana', 1.433);
INSERT INTO country (country_name, multiplier) VALUES ('Gaza Strip', 1.186);
INSERT INTO country (country_name, multiplier) VALUES ('Hong Kong', 1.441);
INSERT INTO country (country_name, multiplier) VALUES ('Heard Island and McDonald Islands', 1.706);
INSERT INTO country (country_name, multiplier) VALUES ('Honduras', 1.459);
INSERT INTO country (country_name, multiplier) VALUES ('Croatia', 1.086);
INSERT INTO country (country_name, multiplier) VALUES ('Haiti', 1.401);
INSERT INTO country (country_name, multiplier) VALUES ('Hungary', 1.079);
INSERT INTO country (country_name, multiplier) VALUES ('Indonesia', 1.556);
INSERT INTO country (country_name, multiplier) VALUES ('Ireland', 1.07);
INSERT INTO country (country_name, multiplier) VALUES ('Israel', 1.189);
INSERT INTO country (country_name, multiplier) VALUES ('Isle of Man', 1.057);
INSERT INTO country (country_name, multiplier) VALUES ('India', 1.351);
INSERT INTO country (country_name, multiplier) VALUES ('British Indian Ocean Territory', 1.467);
INSERT INTO country (country_name, multiplier) VALUES ('Iraq', 1.196);
INSERT INTO country (country_name, multiplier) VALUES ('Iran', 1.225);
INSERT INTO country (country_name, multiplier) VALUES ('Iceland', 1.083);
INSERT INTO country (country_name, multiplier) VALUES ('Italy', 1.103);
INSERT INTO country (country_name, multiplier) VALUES ('Jersey', 1.074);
INSERT INTO country (country_name, multiplier) VALUES ('Jamaica', 1.42);
INSERT INTO country (country_name, multiplier) VALUES ('Jordan', 1.194);
INSERT INTO country (country_name, multiplier) VALUES ('Japan', 1.427);
INSERT INTO country (country_name, multiplier) VALUES ('Kenya', 1.363);
INSERT INTO country (country_name, multiplier) VALUES ('Kyrgyzstan', 1.244);
INSERT INTO country (country_name, multiplier) VALUES ('Cambodia', 1.464);
INSERT INTO country (country_name, multiplier) VALUES ('Kiribati', 1.705);
INSERT INTO country (country_name, multiplier) VALUES ('Comoros', 1.437);
INSERT INTO country (country_name, multiplier) VALUES ('Saint Kitts and Nevis', 1.382);
INSERT INTO country (country_name, multiplier) VALUES ('North Korea', 1.384);
INSERT INTO country (country_name, multiplier) VALUES ('South Korea', 1.407);
INSERT INTO country (country_name, multiplier) VALUES ('Kuwait', 1.225);
INSERT INTO country (country_name, multiplier) VALUES ('Cayman Islands', 1.422);
INSERT INTO country (country_name, multiplier) VALUES ('Kazakhstan', 1.194);
INSERT INTO country (country_name, multiplier) VALUES ('Laos', 1.422);
INSERT INTO country (country_name, multiplier) VALUES ('Lebanon', 1.176);
INSERT INTO country (country_name, multiplier) VALUES ('Saint Lucia', 1.394);
INSERT INTO country (country_name, multiplier) VALUES ('Liechtenstein', 1.073);
INSERT INTO country (country_name, multiplier) VALUES ('Sri Lanka', 1.419);
INSERT INTO country (country_name, multiplier) VALUES ('Liberia', 1.318);
INSERT INTO country (country_name, multiplier) VALUES ('Lesotho', 1.518);
INSERT INTO country (country_name, multiplier) VALUES ('Lithuania', 1.048);
INSERT INTO country (country_name, multiplier) VALUES ('Luxembourg', 1.06);
INSERT INTO country (country_name, multiplier) VALUES ('Latvia', 1.045);
INSERT INTO country (country_name, multiplier) VALUES ('Libya', 1.194);
INSERT INTO country (country_name, multiplier) VALUES ('Morocco', 1.174);
INSERT INTO country (country_name, multiplier) VALUES ('Monaco', 1.093);
INSERT INTO country (country_name, multiplier) VALUES ('Moldova', 1.092);
INSERT INTO country (country_name, multiplier) VALUES ('Montenegro', 1.103);
INSERT INTO country (country_name, multiplier) VALUES ('Madagascar', 1.48);
INSERT INTO country (country_name, multiplier) VALUES ('Marshall Islands', 1.635);
INSERT INTO country (country_name, multiplier) VALUES ('Macedonia [FYROM]', 1.112);
INSERT INTO country (country_name, multiplier) VALUES ('Mali', 1.25);
INSERT INTO country (country_name, multiplier) VALUES ('Myanmar [Burma]', 1.393);
INSERT INTO country (country_name, multiplier) VALUES ('Mongolia', 1.298);
INSERT INTO country (country_name, multiplier) VALUES ('Macau', 1.44);
INSERT INTO country (country_name, multiplier) VALUES ('Northern Mariana Islands', 1.54);
INSERT INTO country (country_name, multiplier) VALUES ('Martinique', 1.39);
INSERT INTO country (country_name, multiplier) VALUES ('Mauritania', 1.239);
INSERT INTO country (country_name, multiplier) VALUES ('Montserrat', 1.383);
INSERT INTO country (country_name, multiplier) VALUES ('Malta', 1.138);
INSERT INTO country (country_name, multiplier) VALUES ('Mauritius', 1.507);
INSERT INTO country (country_name, multiplier) VALUES ('Maldives', 1.421);
INSERT INTO country (country_name, multiplier) VALUES ('Malawi', 1.432);
INSERT INTO country (country_name, multiplier) VALUES ('Mexico', 1.46);
INSERT INTO country (country_name, multiplier) VALUES ('Malaysia', 1.497);
INSERT INTO country (country_name, multiplier) VALUES ('Mozambique', 1.464);
INSERT INTO country (country_name, multiplier) VALUES ('Namibia', 1.475);
INSERT INTO country (country_name, multiplier) VALUES ('New Caledonia', 1.783);
INSERT INTO country (country_name, multiplier) VALUES ('Niger', 1.242);
INSERT INTO country (country_name, multiplier) VALUES ('Norfolk Island', 1.831);
INSERT INTO country (country_name, multiplier) VALUES ('Nigeria', 1.291);
INSERT INTO country (country_name, multiplier) VALUES ('Nicaragua', 1.468);
INSERT INTO country (country_name, multiplier) VALUES ('Netherlands', 1.048);
INSERT INTO country (country_name, multiplier) VALUES ('Norway', 1);
INSERT INTO country (country_name, multiplier) VALUES ('Nepal', 1.328);
INSERT INTO country (country_name, multiplier) VALUES ('Nauru', 1.673);
INSERT INTO country (country_name, multiplier) VALUES ('Niue', 1.795);
INSERT INTO country (country_name, multiplier) VALUES ('New Zealand', 1.906);
INSERT INTO country (country_name, multiplier) VALUES ('Oman', 1.284);
INSERT INTO country (country_name, multiplier) VALUES ('Panama', 1.476);
INSERT INTO country (country_name, multiplier) VALUES ('Peru', 1.548);
INSERT INTO country (country_name, multiplier) VALUES ('French Polynesia', 1.774);
INSERT INTO country (country_name, multiplier) VALUES ('Papua New Guinea', 1.662);
INSERT INTO country (country_name, multiplier) VALUES ('Philippines', 1.509);
INSERT INTO country (country_name, multiplier) VALUES ('Pakistan', 1.277);
INSERT INTO country (country_name, multiplier) VALUES ('Poland', 1.053);
INSERT INTO country (country_name, multiplier) VALUES ('Saint Pierre and Miquelon', 1.229);
INSERT INTO country (country_name, multiplier) VALUES ('Pitcairn Islands', 1.769);
INSERT INTO country (country_name, multiplier) VALUES ('Puerto Rico', 1.388);
INSERT INTO country (country_name, multiplier) VALUES ('Palestinian Territories', 1.185);
INSERT INTO country (country_name, multiplier) VALUES ('Portugal', 1.136);
INSERT INTO country (country_name, multiplier) VALUES ('Palau', 1.568);
INSERT INTO country (country_name, multiplier) VALUES ('Paraguay', 1.574);
INSERT INTO country (country_name, multiplier) VALUES ('Qatar', 1.253);
INSERT INTO country (country_name, multiplier) VALUES ('Réunion', 1.508);
INSERT INTO country (country_name, multiplier) VALUES ('Romania', 1.093);
INSERT INTO country (country_name, multiplier) VALUES ('Serbia', 1.098);
INSERT INTO country (country_name, multiplier) VALUES ('Russia', 1.241);
INSERT INTO country (country_name, multiplier) VALUES ('Rwanda', 1.364);
INSERT INTO country (country_name, multiplier) VALUES ('Saudi Arabia', 1.247);
INSERT INTO country (country_name, multiplier) VALUES ('Solomon Islands', 1.713);
INSERT INTO country (country_name, multiplier) VALUES ('Seychelles', 1.42);
INSERT INTO country (country_name, multiplier) VALUES ('Sudan', 1.281);
INSERT INTO country (country_name, multiplier) VALUES ('Sweden', 1);
INSERT INTO country (country_name, multiplier) VALUES ('Singapore', 1.516);
INSERT INTO country (country_name, multiplier) VALUES ('Saint Helena', 1.49);
INSERT INTO country (country_name, multiplier) VALUES ('Slovenia', 1.08);
INSERT INTO country (country_name, multiplier) VALUES ('Svalbard and Jan Mayen', 1.104);
INSERT INTO country (country_name, multiplier) VALUES ('Slovakia', 1.071);
INSERT INTO country (country_name, multiplier) VALUES ('Sierra Leone', 1.31);
INSERT INTO country (country_name, multiplier) VALUES ('San Marino', 1.091);
INSERT INTO country (country_name, multiplier) VALUES ('Senegal', 1.28);
INSERT INTO country (country_name, multiplier) VALUES ('Somalia', 1.349);
INSERT INTO country (country_name, multiplier) VALUES ('Suriname', 1.429);
INSERT INTO country (country_name, multiplier) VALUES ('São Tomé and Príncipe', 1.342);
INSERT INTO country (country_name, multiplier) VALUES ('El Salvador', 1.473);
INSERT INTO country (country_name, multiplier) VALUES ('Syria', 1.178);
INSERT INTO country (country_name, multiplier) VALUES ('Swaziland', 1.503);
INSERT INTO country (country_name, multiplier) VALUES ('Turks and Caicos Islands', 1.387);
INSERT INTO country (country_name, multiplier) VALUES ('Chad', 1.256);
INSERT INTO country (country_name, multiplier) VALUES ('French Southern Territories', 1.68);
INSERT INTO country (country_name, multiplier) VALUES ('Togo', 1.296);
INSERT INTO country (country_name, multiplier) VALUES ('Thailand', 1.437);
INSERT INTO country (country_name, multiplier) VALUES ('Tajikistan', 1.244);
INSERT INTO country (country_name, multiplier) VALUES ('Tokelau', 1.737);
INSERT INTO country (country_name, multiplier) VALUES ('Timor-Leste', 1.629);
INSERT INTO country (country_name, multiplier) VALUES ('Turkmenistan', 1.21);
INSERT INTO country (country_name, multiplier) VALUES ('Tunisia', 1.149);
INSERT INTO country (country_name, multiplier) VALUES ('Tonga', 1.806);
INSERT INTO country (country_name, multiplier) VALUES ('Turkey', 1.149);
INSERT INTO country (country_name, multiplier) VALUES ('Trinidad and Tobago', 1.41);
INSERT INTO country (country_name, multiplier) VALUES ('Tuvalu', 1.721);
INSERT INTO country (country_name, multiplier) VALUES ('Taiwan', 1.452);
INSERT INTO country (country_name, multiplier) VALUES ('Tanzania', 1.394);
INSERT INTO country (country_name, multiplier) VALUES ('Ukraine', 1.094);
INSERT INTO country (country_name, multiplier) VALUES ('Uganda', 1.348);
INSERT INTO country (country_name, multiplier) VALUES ('U.S. Minor Outlying Islands', 1.562);
INSERT INTO country (country_name, multiplier) VALUES ('United States', 1.376);
INSERT INTO country (country_name, multiplier) VALUES ('Uruguay', 1.613);
INSERT INTO country (country_name, multiplier) VALUES ('Uzbekistan', 1.214);
INSERT INTO country (country_name, multiplier) VALUES ('Vatican City', 1.103);
INSERT INTO country (country_name, multiplier) VALUES ('Saint Vincent and the Grenadines', 1.399);
INSERT INTO country (country_name, multiplier) VALUES ('Venezuela', 1.446);
INSERT INTO country (country_name, multiplier) VALUES ('British Virgin Islands', 1.382);
INSERT INTO country (country_name, multiplier) VALUES ('U.S. Virgin Islands', 1.383);
INSERT INTO country (country_name, multiplier) VALUES ('Vietnam', 1.466);
INSERT INTO country (country_name, multiplier) VALUES ('Vanuatu', 1.755);
INSERT INTO country (country_name, multiplier) VALUES ('Wallis and Futuna', 1.763);
INSERT INTO country (country_name, multiplier) VALUES ('Samoa', 1.765);
INSERT INTO country (country_name, multiplier) VALUES ('Kosovo', 1.105);
INSERT INTO country (country_name, multiplier) VALUES ('Yemen', 1.298);
INSERT INTO country (country_name, multiplier) VALUES ('Mayotte', 1.445);
INSERT INTO country (country_name, multiplier) VALUES ('South Africa', 1.52);
INSERT INTO country (country_name, multiplier) VALUES ('Zambia', 1.425);
INSERT INTO country (country_name, multiplier) VALUES ('Zimbabwe', 1.459);