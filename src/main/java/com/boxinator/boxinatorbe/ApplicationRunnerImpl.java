package com.boxinator.boxinatorbe;

import com.boxinator.boxinatorbe.models.domain.*;
import com.boxinator.boxinatorbe.repositories.AccountRepository;
import com.boxinator.boxinatorbe.repositories.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@Profile("local") // It will only run if environment variable SPRING_PROFILES_ACTIVE=local
public class ApplicationRunnerImpl implements ApplicationRunner {

    //Injections
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Account admin2 = new Account();
        admin2.setFirstName("admin2");
        admin2.setLastName("admin2");
        admin2.setEmail("admin2@keycloak.org");
        admin2.setBirthday(LocalDate.of(1999, 02, 10));
        admin2.setCountry(new Country(2l));
        admin2.setZipCode("123123");
        admin2.setPhoneNumber("9999888");
        admin2.setAccountType(AccountTypeEnum.ADMINISTRATOR);

        Account user = new Account();
        user.setFirstName("Mary");
        user.setLastName("Lee");
        user.setEmail("mary@keycloak.org");
        user.setBirthday(LocalDate.of(1985, 10, 31));
        user.setCountry(new Country(5l));
        user.setZipCode("222333");
        user.setPhoneNumber("99990000");
        user.setAccountType(AccountTypeEnum.REGISTERED_USER);

        Account userTest = new Account();
        userTest.setFirstName("Camila");
        userTest.setLastName("Boaventura");
        userTest.setEmail("camilamboaventura@gmail.com");
        userTest.setBirthday(LocalDate.of(1985, 10, 31));
        userTest.setCountry(new Country(5l));
        userTest.setZipCode("222333");
        userTest.setPhoneNumber("99990000");
        userTest.setAccountType(AccountTypeEnum.REGISTERED_USER);

        Shipment shipmentTest = new Shipment();
        shipmentTest.setCost(2.30);
        shipmentTest.setWeight(2.00);
        shipmentTest.setBoxColour("#879dbc");
        shipmentTest.addStatusHistory(ShipmentStatusEnum.CREATED);
        shipmentTest.setCreatedDate(LocalDate.of(2021, 10, 02));
        shipmentTest.setDestination(new Country(3l));
        shipmentTest.setAccount(userTest);
        shipmentTest.setReceiverName("Batman");

        Shipment shipment = new Shipment();
        shipment.setCost(2.30);
        shipment.setWeight(2.00);
        shipment.setBoxColour("#879dbc");
        shipment.addStatusHistory(ShipmentStatusEnum.CREATED);
        shipment.setCreatedDate(LocalDate.of(2021, 10, 02));
        shipment.setDestination(new Country(2l));
        shipment.setAccount(user);
        shipment.setReceiverName("Superman");

        Shipment shipment1 = new Shipment();
        shipment1.setCost(5.00);
        shipment1.setWeight(1.2);
        shipment1.setBoxColour("#879dbc");
        shipment1.addStatusHistory(ShipmentStatusEnum.CREATED);
        shipment1.setCreatedDate(LocalDate.of(2021, 9, 30));
        shipment1.setDestination(new Country(10l));
        shipment1.setAccount(user);
        shipment.setReceiverName("Padawan");

        accountRepository.save(admin2);
        accountRepository.save(user);
        accountRepository.save(userTest);
        shipmentRepository.save(shipment);
        shipmentRepository.save(shipment1);
        shipmentRepository.save(shipmentTest);

    }

}

