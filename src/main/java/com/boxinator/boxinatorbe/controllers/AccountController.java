package com.boxinator.boxinatorbe.controllers;

import com.boxinator.boxinatorbe.exceptions.ResourceExistsException;
import com.boxinator.boxinatorbe.exceptions.ResourceForbiddenException;
import com.boxinator.boxinatorbe.exceptions.ResourceNotFoundException;
import com.boxinator.boxinatorbe.models.domain.Account;
import com.boxinator.boxinatorbe.models.dtos.AccountDto;
import com.boxinator.boxinatorbe.models.mappers.AccountMapper;
import com.boxinator.boxinatorbe.services.AccountService;
import org.keycloak.KeycloakPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*")

//Account controller class - FULL CRUD for account table
public class AccountController {

    //Injections
    @Autowired
    private AccountMapper mapper;

    @Autowired
    private AccountService service;


    /**
     * Retrieve the account information of a specific user.
     * @param id
     * @param authenticatedUser
     * @return accountDto
     */
    @GetMapping(value = "/account/{id}")
    public ResponseEntity<AccountDto> getAccountById(@PathVariable Long id, @ApiIgnore KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        AccountDto accountDto = service.getAccountById(id, authenticatedUser);
        if (accountDto != null)
            return ResponseEntity.ok(accountDto);
        return ResponseEntity.notFound().build();
    }

    @GetMapping(value = "/account")
    public ResponseEntity<AccountDto> getAccountByEmail(@RequestParam String email, @ApiIgnore KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        AccountDto accountDto = service.getAccountByEmail(email, authenticatedUser);
        if (accountDto != null)
            return ResponseEntity.ok(accountDto);
        return ResponseEntity.notFound().build();
    }

    /**
     * Used to update a specific account.
     * @param id
     * @param accountDto
     * @param authenticatedUser
     * @return
     */
    @PutMapping("/account/{id}")
    public ResponseEntity<AccountDto> updateAccount(@PathVariable Long id, @RequestBody AccountDto accountDto, @ApiIgnore KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        AccountDto dto = service.updateAccount(id, accountDto, authenticatedUser);
        return ResponseEntity.ok(dto);
    }

    /**
     * Used to create a new account.
     * @param accountDto
     * @return savedAccount
     */
    @PostMapping("/account")
    public AccountDto createAccount(@RequestBody AccountDto accountDto, @ApiIgnore KeycloakPrincipal authenticatedUser) throws ResourceExistsException, ResourceForbiddenException {
        Account savedAccount = service.createAccount(accountDto, authenticatedUser);
        return mapper.mapDto(savedAccount);
    }

    /**
     * Delete an account and only accessible by an Administrator.
     * @param accountId
     * @param authenticatedUser
     * @return
     * @throws ResourceNotFoundException
     */
    @DeleteMapping("/account/{id}")
    public ResponseEntity deleteAccount(@PathVariable(value = "id") Long accountId, @ApiIgnore KeycloakPrincipal authenticatedUser)
            throws ResourceNotFoundException, ResourceForbiddenException {
        service.deleteAccount(accountId, authenticatedUser);
        return ResponseEntity.accepted().build();
    }

}
