package com.boxinator.boxinatorbe.repositories;

import com.boxinator.boxinatorbe.models.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Repository for AccountService
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account getByEmail(String email);
}
