package com.boxinator.boxinatorbe.repositories;

import com.boxinator.boxinatorbe.models.domain.Shipment;
import com.boxinator.boxinatorbe.models.domain.ShipmentStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//Repository for ShipmentService
@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Long> {

    //filtering the shipment list by Account email
    List<Shipment> findAllByAccountEmail(String accountEmail);

    //filtering the shipment list by latest status type
    List<Shipment> findAllByLatestStatusStatus(ShipmentStatusEnum status);

    //filtering the shipment list by latest status type and Account Id
    List<Shipment> findAllByLatestStatusStatusAndAccountId(ShipmentStatusEnum status, Long accountId);

}
