package com.boxinator.boxinatorbe.models.dtos;

import com.boxinator.boxinatorbe.models.domain.ShipmentStatusEnum;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.time.LocalDateTime;


public class ShipmentStatusDto {

    ShipmentStatusEnum status;

    LocalDateTime updateTime;


    public ShipmentStatusDto(ShipmentStatusEnum status, LocalDateTime updateTime) {
        this.status = status;
        this.updateTime = updateTime;
    }

    @JsonGetter("status")
    public ShipmentStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ShipmentStatusEnum status) {
        this.status = status;
    }

    @JsonGetter("update_time")
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
