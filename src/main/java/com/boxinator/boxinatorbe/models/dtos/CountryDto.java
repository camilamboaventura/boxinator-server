package com.boxinator.boxinatorbe.models.dtos;

import com.fasterxml.jackson.annotation.JsonGetter;


//DTO for Country class to encapsulate the information to be used in the controller
public class CountryDto {
    private Long id;
    private String countryName;
    private double multiplier;

    //Constructor
    public CountryDto(Long id, String name, double multiplier) {
        this.id = id;
        this.countryName = name;
        this.multiplier = multiplier;
    }

    //Default constructor
    public CountryDto() {
    }

    //GETTERS AND SETTERS WITH @JsonGetter annotation to parse the variables into the JSON format
    @JsonGetter("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonGetter("country_name")
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @JsonGetter("modifier")
    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

}
