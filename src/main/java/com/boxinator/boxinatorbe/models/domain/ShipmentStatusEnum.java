package com.boxinator.boxinatorbe.models.domain;

//Shipment Status
public enum ShipmentStatusEnum {
    CREATED,
    RECEIVED,
    INTRANSIT,
    COMPLETED,
    CANCELLED
}
