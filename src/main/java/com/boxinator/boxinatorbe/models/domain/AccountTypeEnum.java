package com.boxinator.boxinatorbe.models.domain;

//Account types
public enum AccountTypeEnum {
    GUEST,
    REGISTERED_USER,
    ADMINISTRATOR
}
