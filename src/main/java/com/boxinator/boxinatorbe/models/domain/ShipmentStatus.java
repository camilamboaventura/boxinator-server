package com.boxinator.boxinatorbe.models.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "shipment_status")
public class ShipmentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "status")
    ShipmentStatusEnum status;

    @Column(name = "update_time")
    LocalDateTime updateTime;

    public ShipmentStatus(ShipmentStatusEnum status, LocalDateTime updateTime) {
        this.status = status;
        this.updateTime = updateTime;
    }

    public ShipmentStatus() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShipmentStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ShipmentStatusEnum status) {
        this.status = status;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
