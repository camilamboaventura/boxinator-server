package com.boxinator.boxinatorbe.models.mappers;

import com.boxinator.boxinatorbe.models.domain.Country;
import com.boxinator.boxinatorbe.models.dtos.CountryDto;
import org.mapstruct.Mapper;

//Used to map/convert entity to dto and dto to entity
@Mapper(componentModel = "spring")
public interface CountryMapper {
    //convert Entity to a DTO
    CountryDto mapDto(Country country);

    //Convert DTO to an Entity
    Country mapModel(CountryDto country);
}
