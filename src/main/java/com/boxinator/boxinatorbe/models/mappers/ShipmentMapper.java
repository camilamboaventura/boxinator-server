package com.boxinator.boxinatorbe.models.mappers;

import com.boxinator.boxinatorbe.models.domain.Shipment;
import com.boxinator.boxinatorbe.models.dtos.ShipmentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

//Used to map/convert entity to dto and dto to entity
@Mapper(componentModel = "spring")
public interface ShipmentMapper {

    //convert Entity to a DTO
    @Mapping(source = "destination.id", target = "destinationId")
    @Mapping(source = "latestStatus.status", target = "status")
    ShipmentDto mapDto(Shipment shipment);

    //Convert DTO to an Entity
    @Mapping(source = "destinationId", target = "destination.id")
    Shipment mapModel(ShipmentDto shipment);
}
