package com.boxinator.boxinatorbe.services;

import com.boxinator.boxinatorbe.exceptions.ResourceExistsException;
import com.boxinator.boxinatorbe.exceptions.ResourceForbiddenException;
import com.boxinator.boxinatorbe.exceptions.ResourceNotFoundException;
import com.boxinator.boxinatorbe.models.domain.Account;
import com.boxinator.boxinatorbe.models.domain.AccountTypeEnum;
import com.boxinator.boxinatorbe.models.dtos.AccountDto;
import com.boxinator.boxinatorbe.models.mappers.AccountMapper;
import com.boxinator.boxinatorbe.repositories.AccountRepository;
import org.keycloak.KeycloakPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {

    //injecting repository with autowired to automatically generate the constructor
    @Autowired
    private AccountRepository accountRepository;

    //injecting mapper to covert Entity to Dto and Dto to Entity
    @Autowired
    private AccountMapper mapper;

    //get account by Id
    public AccountDto getAccountById(Long id, KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        //getting account in the repository using the id informed as a parameter
        Optional<Account> accountOptional = accountRepository.findById(id);

        //checking if accountOptional contains the account informed as a parameter
        if (accountOptional.isPresent() && account.getId() != accountOptional.get().getId()) {
            throw new ResourceForbiddenException("Restricted access to account owner");
        }

        //checking if account exists
        if (accountOptional.isPresent()) {
            Account accountModel = accountOptional.get();
            //mapping accountModel to dto
            AccountDto accountDto = mapper.mapDto(accountModel);
            //return account
            return accountDto;
        }
        //otherwise return null
        return null;
    }

    //update an account
    public AccountDto updateAccount(Long id, AccountDto accountDto, KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        //mapping accountDto to model
        Account model = mapper.mapModel(accountDto);

        //checking account type
        if (account.getAccountType() != AccountTypeEnum.ADMINISTRATOR) {
            Optional<Account> accountOptional = accountRepository.findById(id);
            //checking if account logged on keycloak is equal to account that will be updated
            if (accountOptional.isPresent() && account.getId() != accountOptional.get().getId()) {
                throw new ResourceForbiddenException("Restricted access to account owner");
            }
            //saving changes
            accountRepository.save(model);
        }

        //saving accountModel in the repository
        Account savedAccount = accountRepository.save(model);

        //mapping account model to a dto
        AccountDto dto = mapper.mapDto(savedAccount);

        //return account with changes
        return dto;
    }

    //create an account
    public Account createAccount(AccountDto accountDto, KeycloakPrincipal authenticatedUser) throws ResourceExistsException, ResourceForbiddenException {
        //getting user email using keycloak security context
        String currentUserEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(currentUserEmail);

        if (account != null) {
            //when guest trying to register themselves
            if (account.getAccountType() == AccountTypeEnum.GUEST) {
                account.setAccountType(AccountTypeEnum.REGISTERED_USER);
            } else {
                //if account already exists return an error
                throw new ResourceExistsException("User already has an account");
            }
        }

        //mapping accountDto to model
        Account model = mapper.mapModel(accountDto);

        // Check if the user is trying to create account using email different from his own
        if(!currentUserEmail.equals(model.getEmail()) && model.getAccountType() != AccountTypeEnum.ADMINISTRATOR){
            throw new ResourceForbiddenException("Only admin can create account to others");
        }

        //saving a new account in the repository
        Account savedAccount = accountRepository.save(model);

        //return new account
        return savedAccount;
    }

    public void deleteAccount(Long accountId, KeycloakPrincipal authenticatedUser) throws ResourceNotFoundException, ResourceForbiddenException {
        Account accountReceived = accountRepository.findById(accountId)
                // Throw account error if not found
                .orElseThrow(() -> new ResourceNotFoundException("Account not found for this id :: " + accountId));

        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting current user account by email
        Account currentUserAccount = accountRepository.getByEmail(accountEmail);

        //checking if the account type is equal to administrator
        if (currentUserAccount.getAccountType() != AccountTypeEnum.ADMINISTRATOR) {
            throw new ResourceForbiddenException("Restricted access to administrator");
        }

        //delete account
        accountRepository.delete(accountReceived);
    }

    public AccountDto getAccountByEmail(String email, KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        Account account = accountRepository.getByEmail(email);
        if(account != null) {

            // Check if the user is trying to get account by email different from his own
            if(!accountEmail.equals(email) && account.getAccountType() != AccountTypeEnum.ADMINISTRATOR){
                throw new ResourceForbiddenException("Only admins can see accounts for others");
            }

            return mapper.mapDto(account);
        }
        return null;
    }
}
