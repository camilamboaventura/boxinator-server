package com.boxinator.boxinatorbe.services;

import com.boxinator.boxinatorbe.exceptions.ResourceForbiddenException;
import com.boxinator.boxinatorbe.exceptions.ResourceNotFoundException;
import com.boxinator.boxinatorbe.models.domain.*;
import com.boxinator.boxinatorbe.models.dtos.ShipmentDto;
import com.boxinator.boxinatorbe.models.mappers.ShipmentMapper;
import com.boxinator.boxinatorbe.repositories.AccountRepository;
import com.boxinator.boxinatorbe.repositories.ShipmentRepository;
import org.keycloak.KeycloakPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ShipmentService {

    //injecting repository with autowired to automatically generate the constructor
    @Autowired
    private ShipmentRepository shipmentRepository;

    @Autowired
    private AccountRepository accountRepository;

    //injecting mapper to covert Entity to Dto and Dto to Entity
    @Autowired
    private ShipmentMapper mapper;

    @Autowired
    private EmailService emailService;

    //get all shipments
    public List<ShipmentDto> getAllShipments(KeycloakPrincipal authenticatedUser) throws ResourceNotFoundException {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        //Return not found if not found
        if (account == null) {
            throw new ResourceNotFoundException("Account not found.");
        }

        List<Shipment> shipments;
        //checking account type
        if (account.getAccountType() == AccountTypeEnum.REGISTERED_USER) {
            //filtering the shipment list by AccountId - accessible by registered user
            //shipments are displayed sorted descending by date
            shipments = shipmentRepository.findAllByAccountEmail(accountEmail).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
        } else {
            //filtering the shipment list by latest status (non-cancelled/non-completed) - accessible by admin
            shipments = shipmentRepository.findAllByLatestStatusStatus(ShipmentStatusEnum.CREATED).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
            shipments.addAll(shipmentRepository.findAllByLatestStatusStatus(ShipmentStatusEnum.INTRANSIT).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList()));
            shipments.addAll(shipmentRepository.findAllByLatestStatusStatus(ShipmentStatusEnum.RECEIVED).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList()));
        }

        List<ShipmentDto> dtoList = new ArrayList<>();

        // Loop through every shipment of response and add to list of DTO
        for (Shipment shipment : shipments) {
            dtoList.add(mapper.mapDto(shipment));
        }
        //return a list of shipments
        return dtoList;
    }

    //get all completed shipments
    public List<ShipmentDto> getAllCompletedShipments(KeycloakPrincipal authenticatedUser) {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        List<Shipment> shipments;

        //checking account type
        if (account.getAccountType() == AccountTypeEnum.REGISTERED_USER) {
            //filtering the shipment list by Status (Completed shipments) and AccountId - registered user
            //shipments are displayed sorted descending by date
            shipments = shipmentRepository.findAllByLatestStatusStatusAndAccountId(ShipmentStatusEnum.COMPLETED, account.getId()).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
            ;
        } else {
            //admin will see all completed shipments
            shipments = shipmentRepository.findAllByLatestStatusStatus(ShipmentStatusEnum.COMPLETED).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
            ;
        }

        List<ShipmentDto> dtoList = new ArrayList<>();

        // Loop through every shipment of response and add to list of DTO
        for (Shipment shipment : shipments) {
            dtoList.add(mapper.mapDto(shipment));
        }

        //return a list of completed shipments
        return dtoList;
    }

    //get all cancelled shipments
    public List<ShipmentDto> getAllCancelledShipments(KeycloakPrincipal authenticatedUser) {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);


        List<Shipment> shipments;

        //checking account type
        if (account.getAccountType() == AccountTypeEnum.REGISTERED_USER) {
            //filtering the shipment list by status(cancelled shipments) and AccountId - registered user
            //shipments are displayed sorted descending by date
            shipments = shipmentRepository.findAllByLatestStatusStatusAndAccountId(ShipmentStatusEnum.CANCELLED, account.getId()).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
        } else {
            //admin will see all completed shipments
            shipments = shipmentRepository.findAllByLatestStatusStatus(ShipmentStatusEnum.CANCELLED).stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
        }

        List<ShipmentDto> dtoList = new ArrayList<>();

        // Loop through every shipment of response and add to list of DTO
        for (Shipment shipment : shipments) {
            dtoList.add(mapper.mapDto(shipment));
        }

        //return a list of cancelled shipments
        return dtoList;
    }

    //create a shipment
    public Shipment createShipment(ShipmentDto shipment, KeycloakPrincipal authenticatedUser) {

        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        Shipment model = mapper.mapModel(shipment);
        //only authenticated user or admin create a new shipment
        if (account.getAccountType() != AccountTypeEnum.ADMINISTRATOR) {
            model.setAccount(account); //the shipment is added to user logged
        }

        //createdDate is automatically generated
        model.setCreatedDate(LocalDate.now());

        //setting status as created
        model.addStatusHistory(ShipmentStatusEnum.CREATED);
        //new shipments are saved in the repository
        Shipment savedShipment = shipmentRepository.save(model);

        //sending a simple mail message to confirm a shipment
        emailService.sendSimpleMessage(accountEmail, "Shipping confirmation", "Your box has been sent.");

        //return a new shipment
        return savedShipment;
    }

    public Shipment createShipmentForGuest(ShipmentDto shipment) throws ResourceForbiddenException {
        String email = shipment.getAccount().getEmail();
        Objects.requireNonNull(email, "Email can't be empty!");

        //getting account by email
        Account account = accountRepository.getByEmail(email);

        if (account == null) {
            account = new Account();
            account.setEmail(email);
            account.setAccountType(AccountTypeEnum.GUEST);
            accountRepository.save(account);
        }

        Shipment model = mapper.mapModel(shipment);
        //only authenticated user or admin create a new shipment
        if (account.getAccountType() != AccountTypeEnum.GUEST) {
            throw new ResourceForbiddenException("Valid only for guest users");
        }
        model.setAccount(account); //the shipment is added to guest user

        //createdDate is automatically generated
        model.setCreatedDate(LocalDate.now());

        //setting status as created
        model.addStatusHistory(ShipmentStatusEnum.CREATED);
        //new shipments are saved in the repository
        Shipment savedShipment = shipmentRepository.save(model);

        //sending a simple mail message to confirm a shipment
        emailService.sendSimpleMessage(account.getEmail(), "Shipping confirmation", "Your box has been sent.");

        //return a new shipment
        return savedShipment;
    }

    //get shipment by id
    public ShipmentDto getShipmentById(Long id, KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        //getting a shipment in the repository using the id informed as a parameter
        Optional<Shipment> shipmentOptional = shipmentRepository.findById(id);

        //checking if shipment exists
        if (shipmentOptional.isPresent()) {
            Shipment shipmentModel = shipmentOptional.get();
            //mapping shipment model in shipment dto
            ShipmentDto shipmentDto = mapper.mapDto(shipmentModel);
            //returns shipment
            return shipmentDto;
        }
        //otherwise return null
        return null;
    }

    //get all customer shipments
    public List<ShipmentDto> getAllCustomerShipments(Long customerId, KeycloakPrincipal authenticatedUser) throws ResourceNotFoundException, ResourceForbiddenException {

        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting logged account by email
        Account currentCustomer = accountRepository.getByEmail(accountEmail);

        //getting account informed as a parameter
        Optional<Account> customerOptional = accountRepository.findById(customerId);

        //Checking if account type is equal to administrator
        if (currentCustomer.getAccountType() != AccountTypeEnum.ADMINISTRATOR) {
            //checking if the keycloak logged account contains the account informed as a parameter
            if (customerOptional.isPresent() && currentCustomer.getId() != customerOptional.get().getId()) {
                throw new ResourceForbiddenException("Restricted access. Users can only view their own shipments");
            }
        }

        List<Shipment> shipments;

        //if customer exists, retrieve their shipments
        if (customerOptional.isPresent()) {
            //shipments are displayed sorted descending by date
            shipments = customerOptional.get().getShipments().stream()
                    .sorted(Comparator.comparing(Shipment::getCreatedDate).reversed())
                    .collect(Collectors.toList());
        } else {
            //if customer doesn't exist throw an error and a message
            throw new ResourceNotFoundException("Customer not found.");
        }

        List<ShipmentDto> dtoList = new ArrayList<>();

        //Loop through every shipment of response and add to list of DTO
        for (Shipment shipment : shipments) {
            //add shipment (mapped to dto) to dtoList
            dtoList.add(mapper.mapDto(shipment));
        }

        //
        return dtoList;
    }

    //update a shipment
    public ShipmentDto updateShipment(Long id, ShipmentDto shipment, KeycloakPrincipal authenticatedUser) throws ResourceForbiddenException {
        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        //mapping shipmentDto to model
        Shipment model = mapper.mapModel(shipment);

        //the user can only change the status to cancelled
        if (account.getAccountType() != AccountTypeEnum.ADMINISTRATOR) {
            Optional<Shipment> shipmentOptional = shipmentRepository.findById(id);
            if (shipmentOptional.isPresent() && account.getId() != shipmentOptional.get().getAccount().getId()) {
                throw new ResourceForbiddenException("Restricted access to account owner.");
            }
            model = shipmentOptional.get();
            //setting status to cancelled
            model.addStatusHistory(ShipmentStatusEnum.CANCELLED);
        } else {
            model.addStatusHistory(shipment.getStatus());
        }
        //saving shipmentModel with the respective changes
        Shipment savedModel = shipmentRepository.save(model);

        //mapping savedModel to dto
        ShipmentDto dto = mapper.mapDto(savedModel);

        //return a shipment with changes
        return dto;
    }

    //delete a shipment
    public void deleteShipment(Long shipmentId, KeycloakPrincipal authenticatedUser) throws ResourceNotFoundException, ResourceForbiddenException {
        Shipment shipment = shipmentRepository.findById(shipmentId)
                // Throw shipment error if not found
                .orElseThrow(() -> new ResourceNotFoundException("Shipment not found for this id :: " + shipmentId));

        //getting user email using keycloak security context
        String accountEmail = authenticatedUser.getKeycloakSecurityContext().getToken().getEmail();

        //getting account by email
        Account account = accountRepository.getByEmail(accountEmail);

        //the user can only change the status to cancelled
        if (account.getAccountType() != AccountTypeEnum.ADMINISTRATOR) {
            throw new ResourceForbiddenException("Restricted access to administrator");
        }

        shipmentRepository.delete(shipment);
    }
}
